﻿using System.Linq;
using UnityEngine;

public class FruitHandler : MonoBehaviour
{
    private Animator animator;
    public bool isNew = true;

    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        //transform.position = Camera.main.ViewportToWorldPoint(new Vector3(Random.Range(0.09f, 0.9f), Random.Range(0.12f, 0.89f), Camera.main.nearClipPlane + 1f));
    }

    void FixedUpdate()
    {
        /*var collectablesToSetVisible = GameObject.FindGameObjectsWithTag("Collectable").Where(c => c.GetComponent<SpriteRenderer>().color.a != 255 && c.GetComponent<FruitHandler>().isNew == false);

        var collectablesToSetFalse = GameObject.FindGameObjectsWithTag("Collectable").Where(c => c.GetComponent<SpriteRenderer>().color.a != 255 && c.GetComponent<FruitHandler>().isNew == true); ;
        foreach (var c in collectablesToSetVisible)
        {
            Destroy(c.gameObject);
        }
        foreach (var c in collectablesToSetVisible)
        {
            c.GetComponent<SpriteRenderer>().color = new Color(255, 255, 255, 255);
        }

        Debug.Log(isNew);*/

    }

    void OnTriggerEnter2D(Collider2D collider2D)
    {
        isNew = false;
        Debug.Log("Passou no enter");
        if (collider2D.gameObject.CompareTag("Player"))
        {
            animator.SetBool("Collected", true);

            Destroy(this.gameObject, 0.25f);
        }

        if (collider2D.gameObject.CompareTag("Grounds") || collider2D.gameObject.CompareTag("Walls&Ceil"))
        {
            Destroy(this.gameObject);
        }
    }

    void OnTriggerStay2D(Collider2D collider2D)
    {
        if (collider2D.gameObject.CompareTag("Grounds") || collider2D.gameObject.CompareTag("Walls&Ceil"))
        {
            Destroy(this.gameObject);
        }
    }
}
